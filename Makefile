# vim: set noet sw=4 ts=4 ft=make:

SHELL = /bin/sh



.PHONY: init build clean

build: init sj224.mods.xp

init:

clean: clean_sj224.mods.xp




# use only for boostrap of new mod?
FVER = 1.12.2-14.23.4.2705

./forge: ./forge-${FVER}-mdk.zip
	rm -rf forge
	mkdir forge
	unzip forge-${FVER}-mdk.zip -d forge

./forge-${FVER}-mdk.zip:
	wget "http://files.minecraftforge.net/maven/net/minecraftforge/forge/${FVER}/forge-${FVER}-mdk.zip"




.PHONY: sj224.mods.xp clean_sj224.mods.xp
sj224.mods.xp:
	cd sj224.mods.xp && make build
clean_sj224.mods.xp:
	cd sj224.mods.xp && make clean
