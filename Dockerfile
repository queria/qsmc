# image used for building this minecraft mod
FROM openjdk:alpine

RUN apk update && apk add make bash && rm -rf /var/cache/apk/APKINDEX.*
COPY sj224.mods.xp/gradle /gradle
COPY sj224.mods.xp/gradlew /gradlew
RUN /gradlew
COPY sj224.mods.xp/build.gradle /build.gradle
RUN /gradlew setupCIWorkspace
RUN rm /build.gradle
CMD /bin/sh
