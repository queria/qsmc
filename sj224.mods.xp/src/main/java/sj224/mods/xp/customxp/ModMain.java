////////
//
// Code forked from https://gist.github.com/SuperJedi224/d3b0c7c92f2d6cbe242b5f9d0fbef2ba
// Original author is *SuperJedi224*
// Homepage of this mod is https://minecraft.curseforge.com/projects/configurable-xp-drops
// Creative Commons Attribution-Share Alike License
//
////////

package sj224.mods.xp;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.boss.EntityDragon;
import net.minecraft.entity.monster.EntityPigZombie;
import net.minecraft.entity.monster.EntitySlime;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.passive.AbstractHorse;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.passive.EntityHorse;
import net.minecraft.entity.passive.EntitySkeletonHorse;
import net.minecraft.init.Blocks;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.event.entity.living.LivingExperienceDropEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod(modid = ModMain.MODID, version = ModMain.VERSION)
public class ModMain
{
    public static final String MODID = "customxp";
    public static final String VERSION = "1.1_qs1";
    
    static Map<String,Integer>xp=new HashMap<String, Integer>();
    
    static Configuration cfg;
    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
    	Configuration cfg=new Configuration(event.getSuggestedConfigurationFile());
        xp.put("bat",cfg.getInt("bat","mobs",0,0,9999,""));
        xp.put("chicken",cfg.getInt("chicken","mobs",2,0,9999,""));
        xp.put("cow",cfg.getInt("cow","mobs",2,0,9999,""));
        xp.put("mooshroom",cfg.getInt("mooshroom","mobs",2,0,9999,""));
        xp.put("pig",cfg.getInt("pig","mobs",2,0,9999,""));
        xp.put("rabbit",cfg.getInt("rabbit","mobs",2,0,9999,""));
        xp.put("sheep",cfg.getInt("sheep","mobs",2,0,9999,""));
        xp.put("horse",cfg.getInt("horse","mobs",2,0,9999,""));
        xp.put("skeleton_horse",cfg.getInt("horse","mobs",2,0,9999,""));
        xp.put("llama",cfg.getInt("llama","mobs",2,0,9999,""));
        xp.put("ocelot",cfg.getInt("ocelot","mobs",2,0,9999,""));
        xp.put("wolf",cfg.getInt("wolf","mobs",2,0,9999,""));
        xp.put("squid",cfg.getInt("squid","mobs",2,0,9999,""));
        xp.put("cave_spider",cfg.getInt("cave_spider","mobs",5,0,9999,""));
        xp.put("enderman",cfg.getInt("enderman","mobs",5,0,9999,""));
        xp.put("cave_spider",cfg.getInt("cave_spider","mobs",5,0,9999,""));
        xp.put("polar_bear",cfg.getInt("polar_bear","mobs",2,0,9999,""));
        xp.put("cave_spider",cfg.getInt("cave_spider","mobs",5,0,9999,""));
        xp.put("zombie_pigman",cfg.getInt("pigman","mobs",5,0,9999,""));
        xp.put("zombie_pigman_baby",cfg.getInt("baby_pigman","mobs",12,0,9999,""));
        xp.put("zombie",cfg.getInt("zombie","mobs",5,0,9999,""));
        xp.put("zombie_baby",cfg.getInt("baby_zombie","mobs",12,0,9999,""));
        xp.put("zombie_villager",cfg.getInt("zombie","mobs",5,0,9999,""));
        xp.put("zombie__villager_baby",cfg.getInt("baby_zombie","mobs",12,0,9999,""));
        xp.put("husk",cfg.getInt("husk","mobs",5,0,9999,""));
        xp.put("husk_baby",cfg.getInt("baby_husk","mobs",12,0,9999,""));
        xp.put("blaze",cfg.getInt("blaze","mobs",10,0,9999,""));
        xp.put("creeper",cfg.getInt("creeper","mobs",5,0,9999,""));
        xp.put("elder_guardian",cfg.getInt("elder_guardian","mobs",10,0,9999,""));
        xp.put("endermite",cfg.getInt("endermite","mobs",3,0,9999,""));
        xp.put("evocation_illager",cfg.getInt("evoker","mobs",10,0,9999,""));
        xp.put("vex",cfg.getInt("vex","mobs",3,0,9999,""));
        xp.put("vindication_illager",cfg.getInt("vindicator","mobs",5,0,9999,""));
        xp.put("ghast",cfg.getInt("ghast","mobs",5,0,9999,""));
        xp.put("guardian",cfg.getInt("guardian","mobs",10,0,9999,""));
        xp.put("big_slime",cfg.getInt("big_slime","mobs",4,0,9999,""));
        xp.put("big_cube",cfg.getInt("big_magma_cube","mobs",4,0,9999,""));
        xp.put("big_magma_cube",cfg.getInt("big_magma_cube","mobs",4,0,9999,""));
        xp.put("shulker",cfg.getInt("shulker","mobs",5,0,9999,""));
        xp.put("silverfish",cfg.getInt("silverfish","mobs",5,0,9999,""));
        xp.put("skeleton",cfg.getInt("skeleton","mobs",5,0,9999,""));
        xp.put("wither_skeleton",cfg.getInt("wither_skeleton","mobs",5,0,9999,""));
        xp.put("stray",cfg.getInt("stray","mobs",5,0,9999,""));
        xp.put("witch",cfg.getInt("witch","mobs",5,0,9999,""));
        xp.put("wither",cfg.getInt("wither","mobs",50,0,9999,""));
        xp.put("dragon",cfg.getInt("ender_dragon","mobs",500,0,9999,""));
        xp.put("villager_golem",cfg.getInt("iron_golem","mobs",0,0,9999,""));
        xp.put("snowman",cfg.getInt("snow_golem","mobs",0,0,9999,""));
        cfg.save();
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event){
    	MinecraftForge.EVENT_BUS.register(this);
    }
    @SubscribeEvent
    public void dropXP(LivingExperienceDropEvent e){
    	if(!(e.getEntityLiving() instanceof EntityLiving))return;
    	EntityLiving en=(EntityLiving)e.getEntityLiving();
    	String i=EntityList.getKey(en).getResourcePath();
    	if(EntityList.getKey(e.getEntityLiving()).getResourceDomain().equals("minecraft")){
            System.out.println("respath/entity: " + i + "/" + en);
    		if(en instanceof EntitySlime){
    			int siz=((EntitySlime)en).getSlimeSize();
    			int xpc=xp.get("big_"+i);
                if(siz==2)xpc/=1.8;
                if(siz==1)xpc/=3;
    			e.setDroppedExperience(xpc);return;
    		}else if(en instanceof EntityZombie){
    			int xpc=xp.get(i);
        		if(e.getOriginalExperience()==12){
        			xpc=xp.get(i+"_baby");        			
        		}
        		e.setDroppedExperience(xpc);return;
        	}else if((en instanceof AbstractHorse)&&!(en instanceof EntitySkeletonHorse)){
        		i="horse";
        	}else if(en instanceof EntityAnimal){
        		if(((EntityAnimal)en).getGrowingAge()<0){
        			return;
        		}
        	}else if(en instanceof EntityDragon){
        		int xpc=xp.get("dragon");
        		if(e.getOriginalExperience()==12000){
        			xpc*=20;
        		}
        		e.setDroppedExperience(xpc);return;
        	}
    		e.setDroppedExperience(xp.getOrDefault(i,e.getOriginalExperience()));
    	}
    	
    }
    
    
}
